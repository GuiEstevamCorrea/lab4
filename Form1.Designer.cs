﻿using System.Media;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace Lab4
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Text = "Form1";
        }


        private PictureBox spritePictureBox;
        private Timer moveTimer;
        private SoundPlayer soundPlayer;

        private void InitializeSprite()
        {
            spritePictureBox = new PictureBox
            {
                Image = Image.FromFile("/pngwing.com.png"),
                SizeMode = PictureBoxSizeMode.AutoSize,
                Location = new Point(100, 100)
            };

            Controls.Add(spritePictureBox);
        }

        private void InitializeSound()
        {
            soundPlayer = new SoundPlayer("/production-elements-impactor-e-188986.mp3");
        }

        private void InitializeTimer()
        {
            moveTimer = new Timer
            {
                Interval = 50
            };

            moveTimer.Tick += MoveTimer_Tick;
            moveTimer.Start();
        }

        private void MoveTimer_Tick(object sender, EventArgs e)
        {
            int moveDistance = 5;
            int maxY = ClientSize.Height - spritePictureBox.Height;

            if (spritePictureBox.Top + moveDistance <= maxY)
            {
                spritePictureBox.Top += moveDistance;
                PlaySound();
            }
            else
            {
                spritePictureBox.Top = maxY;
                moveDistance = -moveDistance;
            }

            if (spritePictureBox.Top + moveDistance >= 0)
            {
                spritePictureBox.Top += moveDistance;
                PlaySound();
            }
            else
            {
                spritePictureBox.Top = 0;
                moveDistance = -moveDistance;
            }
        }


        private void PlaySound()
        {
            soundPlayer.Play();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeSprite();
            InitializeSound();
            InitializeTimer();
        }

        #endregion
    }
}
